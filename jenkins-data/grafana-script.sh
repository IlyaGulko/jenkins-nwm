#!/usr/bin/env bash

sudo /usr/bin/add-apt-repository "deb https://packages.grafana.com/oss/deb stable main"
curl https://packages.grafana.com/gpg.key | apt-key add -
sudo apt-get update
sudo apt-get install grafana
sudo service grafana-server start
